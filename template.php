<?php

function leanzine_preprocess_html(&$vars) {

  if (theme_get_setting('leanzine_width') == '960') {
    $vars['classes_array'][] = 'c960';
    drupal_add_css(path_to_theme() . '/css/c960.css', array('group' => CSS_THEME, 'preprocess' => FALSE));
  } else {
    $vars['classes_array'][] = 'c96';
    drupal_add_css(path_to_theme() . '/css/c96.css', array('group' => CSS_THEME, 'preprocess' => FALSE));
  }
  
  if (theme_get_setting('leanzine_mode') == 'dev'){
    $vars['classes_array'][] = 'show-grid';
    $vars['classes_array'][] = 'dev';
  }
}

function leanzine_preprocess_page(&$vars) {
  file_put_contents('html.txt',print_r($vars,true));
}

