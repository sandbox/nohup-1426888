<?php

/**
 * @file
 * Theme setting callbacks for the leanzine theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function leanzine_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['leanzine_width'] = array(
    '#type' => 'select',
    '#title' => t('Content width'),
    '#options' => array(
      '96p' => t('96%'),
      '960' => t('960 pixel'),
    ),
    '#default_value' => theme_get_setting('leanzine_width'),
    '#description' => t('Specifiy the maximum width of the'),
  );
  $form['leanzine_mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'),
    '#options' => array(
      'prod' => t('Production'),
      'dev' => t('Development'),
    ),
    '#default_value' => theme_get_setting('leanzine_mode'),
    '#description' => t('Specifiy the theme mode'),
  );
}

